package com.promantia.sharafdg.feedback;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.retail.posterminal.POSUtils;

public class FeedbackComponentProvider extends BaseComponentProvider {

  // public static final String QUALIFIER = "PUPI_Main";
  public static final String MODULE_JAVA_PACKAGE = "com.promantia.sharafdg.feedback";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final GlobalResourcesHelper grhelper = new GlobalResourcesHelper();

    grhelper.add("hook/FeedbackHook.js");
    grhelper.add("hook/FeedbackChangeBPHook.js");
    grhelper.add("hook/FeedbackChangeProductHook.js");
    grhelper.add("hook/FeedbackDeleteProductHook.js");
    grhelper.add("hook/Feedback_PreOrderSave.js");
    grhelper.add("hook/FeedbackDeleteOrderHook.js");
    grhelper.add("hook/Feedback_RemovePaymentHook.js");
    grhelper.add("hook/FeedbackSwitchTicketsHook.js");
    grhelper.add("hook/feedback_LoadPOSWindowHook.js");
    grhelper.add("extend/extendsalesrepresentative.js");
    grhelper.add("extend/extendSalesRepresentativePopup.js");

    List<ComponentResource> resources = grhelper.getGlobalResources();

    return resources;
  }

  private class GlobalResourcesHelper {
    private final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    private final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";

    public void add(String file) {
      globalResources.add(
          createComponentResource(ComponentResourceType.Static, prefix + file, POSUtils.APP_NAME));

    }

    public List<ComponentResource> getGlobalResources() {
      return globalResources;
    }
  }
}
