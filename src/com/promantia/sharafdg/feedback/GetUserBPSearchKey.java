package com.promantia.sharafdg.feedback;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.businesspartner.BusinessPartner;

public class GetUserBPSearchKey extends JSONProcessSimple{
	
	private static final Logger log = Logger.getLogger(GetUserBPSearchKey.class);

	@Override
	public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {

		String bpID = jsonsent.getString("bpId");	
		String bpSearchKey = "";
		JSONObject data = new JSONObject();
		JSONObject retJSON = new JSONObject();
		
		try {
			OBContext.setAdminMode(true);
			BusinessPartner BPObj = OBDal.getInstance().get(BusinessPartner.class, bpID);
			bpSearchKey = BPObj.getSearchKey();
			data.put("bpsearchKey", bpSearchKey);
		} catch (Exception exc) {
			log.error(exc);
			exc.printStackTrace();
		} finally {
			OBContext.restorePreviousMode();
		}

		retJSON.put("status", 0);
		retJSON.put("message", "success");
		retJSON.put("data", data);
		return retJSON;
	}


}
