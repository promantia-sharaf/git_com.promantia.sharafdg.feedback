package com.promantia.sharafdg.feedback.extend;

import java.util.Arrays;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.master.SalesRepresentative;

@Qualifier(SalesRepresentative.salesRepresentativePropertyExtension)
public class NewSalesRepresentativeProperties extends ModelExtension {

	@Override
	public List<HQLProperty> getHQLProperties(Object params) {
		return Arrays.asList(
	            new HQLProperty("user.businessPartner.searchKey", "bpSearchKey")); 
		}

}
