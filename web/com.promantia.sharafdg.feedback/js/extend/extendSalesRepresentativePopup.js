/**
 * 
 */

SHASR.UI.SalesRepresentativePopup.extend({
	
	  setSalesRep: function (inSender, inEvent) {
			var me = this;
		    this.changedSalesRep = true;
		    this.shasrSalesrep = inEvent.id;
		    this.shasrSalesrepName = inEvent.name;
		    this.shasrSalesrepUserName = inEvent.username;
		    if(inEvent.id === null){
		    	this.shasrSalesrepUserBPSk = '';
		    }
		    
		    function errorCallback(tx, error) {
		        OB.error(tx);
		        OB.error(error);
		      }

		      function successCallbackBPs(dataBps) {
		    	  me.shasrSalesrepUserBPSk = dataBps.get('bpSearchKey');

		      }
		    OB.Dal.get(OB.Model.SalesRepresentative, inEvent.id, successCallbackBPs, errorCallback);
		      
		  },
		  applyChanges: function (inSender, inEvent) {
			    if (OB.MobileApp.model.hasPermission('OBPOS_SR.comboOrModal', true)) {
			      if (this.changedSalesRep) {
			        var i;
			        for (i = 0; i < this.args.selectedModels.length; i++) {
			          this.args.selectedModels[i].set('shasrSalesrep', this.shasrSalesrep);
			          this.args.selectedModels[i].set('shasrSalesrepName', this.shasrSalesrepName);
			          this.args.selectedModels[i].set('shasrSalesrepUserName', this.shasrSalesrepUserName);
			          this.args.selectedModels[i].set('shasrSalesrepUserBPSk', this.shasrSalesrepUserBPSk);
			        }
			      }
			    } else {
			      this.waterfall('onApplyChange', {});
			    }
			    return true;
			  }
});