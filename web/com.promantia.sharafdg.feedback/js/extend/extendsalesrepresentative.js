/**
 * 
 */
//To add user's linked business partner search key of selected sales representative. 

OB.Model.SalesRepresentative.addProperties([{
	    name: 'bpSearchKey',
	    column: 'bpSearchKey',
	    type: 'TEXT'
	  }]);

OB.Model.SalesRepresentative.addIndex([{
    name: 'inx_bpSearchKey',
    columns: [{
      name: 'bpSearchKey'
    }]
  }]);