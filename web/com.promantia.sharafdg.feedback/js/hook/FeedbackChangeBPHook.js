OB.UTIL.HookManager.registerHook('OBPOS_preChangeBusinessPartner', function (args, callbacks) {

  if (localStorage.getItem('receipt')) {
    localStorage.setItem('receipt_backup', localStorage.getItem('receipt'));
  }

  var productArr = [],
      count = 0;
  var order = OB.MobileApp.view.terminal.receipt;
  order.get('lines').models.forEach(function (line) {
    OB.CPSF.Utils.getBrandAndDepartment(line, function (note) {
      var productObj = {
        "sku": line.get('product').get('searchkey'),
        "productname": line.get('product').get('_identifier'),
        "brand": note.brand,
        "dept_code": note.department,
        "dept_name": note.departmentName,
        "quantity": line.get('qty')
      };
      productArr.push(productObj);
      count++;
      if (order.get('lines').models.length === count) {
        getOrderJSONForBPChange(args, order, productArr, vat);
      }
    });
  });

  var totalGross = 0,
      vat = 0;
  var lines = order.get('lines').models;

  for (var l in lines) {
    totalGross += lines[l].get('gross');
    vat += lines[l].get('taxAmount');
  }

  getOrderJSONForBPChange = function (args, order, productArr, vat) {
    var bpObj = {
      "name": args.bp.get('name'),
      "cashiername": OB.MobileApp.view.terminal.get('context').user.name,
      "mobile": args.bp.get('phone'),
      "category": args.bp.get('businessPartnerCategory_name'),
      "address": args.bp.get('locName'),
      "email": args.bp.get('email'),
      "products": productArr,
      "total": order.get('gross'),
      "vat": vat,
      "id_customer": args.bp.get('id'),
      "id_terminal": OB.MobileApp.view.terminal.receipt.get('posTerminal'),
      "id_organization": args.bp.get('organization')
    };

    localStorage.setItem('receipt', JSON.stringify(bpObj));
  };
  localStorage.setItem('refresh', '0');
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});