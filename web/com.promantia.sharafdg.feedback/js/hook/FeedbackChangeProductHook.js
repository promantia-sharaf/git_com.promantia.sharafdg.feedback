OB.UTIL.HookManager.registerHook('OBPOS_PostAddProductToOrder', function (args, callbacks) {

  if (localStorage.getItem('receipt')) {
    localStorage.setItem('receipt_backup', localStorage.getItem('receipt'));
  }

  var prodArr = [];
  args.receipt.get('lines').models.forEach(function (line) {
    OB.CPSF.Utils.getBrandAndDepartment(line, function (note) {
      var productObj = {
        "sku": line.get('product').get('searchkey'),
        "productname": line.get('product').get('_identifier'),
        "brand": note.brand,
        "dept_code": note.department,
        "dept_name": note.departmentName,
        "quantity": line.get('qty')
      };
      prodArr.push(productObj);
    });
  });

  var totalGross = 0,
      vat = 0,
      taxableAmt = 0,
      isBulkSales = false;
  var lines = args.receipt.get('lines').models;
  for (var l in lines) {
    if (!OB.UTIL.isNullOrUndefined(lines[l].get('originalDocumentNo'))) {
      if (lines[l].get('originalDocumentNo').startsWith('BS')) {
        isBulkSales = true;
      }
      if (!lines[l].get('isVerifiedReturn')) {
        totalGross += lines[l].get('gross');
        vat += lines[l].get('taxAmount');
      } else {
        var taxes = lines[l].get('taxes');
        for (var t in taxes) {
          vat += taxes[t].taxAmount;
          taxableAmt += taxes[t].taxableAmount;
        }
        if (isBulkSales) {
          var unitTaxableAmt = taxableAmt / lines[l].get('quantity');
          var unitVat = vat / lines[l].get('quantity');
          taxableAmt = unitTaxableAmt * lines[l].get('qty');
          vat = unitVat * lines[l].get('qty');
        }
      }
    }
  }

  if (!(isBulkSales) && taxableAmt > 0) {
    totalGross = (-1) * (taxableAmt + vat);
    vat = (-1) * vat;
  } else if (isBulkSales && taxableAmt < 0) {
    totalGross = taxableAmt + vat;
  }

  var finalObj = {
    "name": args.receipt.get('bp').get('name'),
    "cashiername": OB.MobileApp.view.terminal.get('context').user.name,
    "mobile": args.receipt.get('bp').get('phone'),
    "category": args.receipt.get('bp').get('businessPartnerCategory_name'),
    "address": args.receipt.get('bp').get('locName'),
    "email": args.receipt.get('bp').get('email'),
    "products": prodArr,
    "total": totalGross,
    "vat": vat,
    "id_customer": args.receipt.get('bp').get('id'),
    "id_terminal": args.receipt.get('posTerminal'),
    "id_organization": args.receipt.get('bp').get('organization')
  };

  localStorage.setItem('receipt', JSON.stringify(finalObj));
  localStorage.setItem('refresh', '0');
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});

window.addEventListener("beforeunload", function () {
  localStorage.setItem('refresh', '1');
}, true);