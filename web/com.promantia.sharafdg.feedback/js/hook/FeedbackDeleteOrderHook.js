OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteCurrentOrder', function (args, callbacks) {
	
	localStorage.removeItem('receipt');
	localStorage.setItem('refresh', '0');
	OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});