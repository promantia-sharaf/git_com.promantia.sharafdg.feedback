OB.UTIL.HookManager.registerHook('OBPOS_PostDeleteLine', function (args, callbacks) {

  if (localStorage.getItem('receipt')) {
    localStorage.setItem('receipt_backup', localStorage.getItem('receipt'));
  }

  var receipt = args.order,
      count = 0;
  var prodObj, bpObj;
  if (receipt.get('lines').length > 0) {
    var productArr = [];
    receipt.get('lines').models.forEach(function (prod) {
      OB.CPSF.Utils.getBrandAndDepartment(prod, function (note) {
        prodObj = {
          "sku": prod.get('product').get('searchkey'),
          "productname": prod.get('product').get('_identifier'),
          "brand": note.brand,
          "dept_code": note.department,
          "dept_name": note.departmentName,
          "quantity": prod.get('qty')
        };
        productArr.push(prodObj);
        count++;
        if (receipt.get('lines').length === count) {
          getDeleteOrderJSON(receipt, productArr, vat);
        }
      });
    });

    var vat = 0;
    var taxes = receipt.get('taxes');
    for (var t in taxes) {
      vat += taxes[t].amount;
    }

    getDeleteOrderJSON = function (receipt, productArr, vat) {
      bpObj = {
        "name": receipt.get('bp').get('name'),
        "cashiername": OB.MobileApp.view.terminal.get('context').user.name,
        "mobile": receipt.get('bp').get('phone'),
        "category": receipt.get('bp').get('businessPartnerCategory_name'),
        "address": receipt.get('bp').get('locName'),
        "email": receipt.get('bp').get('email'),
        "products": productArr,
        "total": receipt.get('gross'),
        "vat": vat,
        "id_customer": receipt.get('bp').get('id'),
        "id_terminal": receipt.get('posTerminal'),
        "id_organization": receipt.get('bp').get('organization')
      };


      localStorage.setItem('receipt', JSON.stringify(bpObj));
    };
  } else {
    bpObj = {};
    prodObj = {};
  }
  localStorage.setItem('refresh', '0');
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});