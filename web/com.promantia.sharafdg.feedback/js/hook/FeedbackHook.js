OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, c) {

  if (localStorage.getItem('receipt')) {
    localStorage.setItem('receipt_backup', localStorage.getItem('receipt'));
  }

  var productArr = [];
  var salesExecBPSK = OB.MobileApp.view.terminal.get('context').user.businessPartnerSearchKey,
      count = 0;

  OB.CPSF = OB.CPSF || {};

  OB.CPSF.Utils = {

    getBrandAndDepartment: function (prod, callback) {
      var note = [];
      OB.Dal.find(OB.Model.Brand, {
        id: prod.get('product').get('brand')
      }, function (brand) {
        if (brand.length === 1) {
          note.brand = brand.at(0).get('name');
        } else {
          note.brand = '-';
        }
      });
      OB.CUSTSHA.findRootId({
        categoryId: prod.get('product').get('productCategory'),
        parentId: prod.get('product').get('productCategory')
      }).then(function (data) {
        note.department = data.category.get('searchKey');
        note.departmentName = data.category.get('name');
        callback(note);
      }, function (err) {
        note.department = '-';
        note.departmentName = '-';
        callback(note);
      });
    }
  };

  var vat = 0;
  var taxes = args.context.get('order').get('taxes');
  for (var t in taxes) {
    vat += taxes[t].amount;
  }

  args.context.get('order').get('lines').models.forEach(function (prod) {
    OB.CPSF.Utils.getBrandAndDepartment(prod, function (note) {
      var prodObj = {
        "sku": prod.get('product').get('searchkey'),
        "productname": prod.get('product').get('_identifier'),
        "brand": note.brand,
        "dept_code": note.department,
        "dept_name": note.departmentName,
        "quantity": prod.get('qty'),
        "salesexecempid": prod.get('shasrSalesrepUserBPSk') ? prod.get('shasrSalesrepUserBPSk').split('-')[0] : salesExecBPSK
      };
      productArr.push(prodObj);
      count++;
      if (args.context.get('order').get('lines').models.length === count) {
        getOrderJSON(args, productArr, vat);
      }
    });
  });

  getOrderJSON = function (args, productArr, vat) {
    var bpObj = {
      "name": args.context.get('order').get('bp').get('name') ? args.context.get('order').get('bp').get('name') : args.context.get('order').get('bp').get('firstName'),
      "cashiername": OB.MobileApp.view.terminal.get('context').user.name,
      "mobile": args.context.get('order').get('bp').get('phone'),
      "category": args.context.get('order').get('bp').get('businessPartnerCategory_name'),
      "address": args.context.get('order').get('bp').get('locName'),
      "email": args.context.get('order').get('bp').get('email'),
      "invoicenumber": args.context.get('order').get('documentNo'),
      "invoicedate": moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD'),
      "products": productArr,
      "total": args.context.get('order').get('gross'),
      "vat": vat,
      "id_customer": args.context.get('order').get('bp').get('id'),
      "id_terminal": args.context.get('order').get('posTerminal'),
      "id_organization": args.context.get('order').get('bp').get('organization'),
      "organization": OB.MobileApp.view.terminal.get('context').organization.searchKey,
      "cashierempid": OB.MobileApp.view.terminal.get('context').user.businessPartnerSearchKey
    };
    localStorage.setItem('refresh', '0');
    localStorage.setItem('receipt', JSON.stringify(bpObj));

    //Multicast object to send
    var multicast_date = {
      "name": args.context.get('order').get('bp').get('name') ? args.context.get('order').get('bp').get('name') : args.context.get('order').get('bp').get('firstName'),
      "mobile": args.context.get('order').get('bp').get('phone'),
      "email": args.context.get('order').get('bp').get('email'),
      "address": args.context.get('order').get('bp').get('locName'),
      "bp_category": args.context.get('order').get('bp').get('businessPartnerCategory_name'),
      "invoice_number": args.context.get('order').get('documentNo'),
      "invoice_date": moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD'),
      "products": productArr,
      "currency": OB.MobileApp.model.get('currency').iSOCode,
      "total": args.context.get('order').get('gross'),
      "tax": vat,
      "organization": OB.MobileApp.view.terminal.get('context').organization.searchKey,
      "terminal": OB.MobileApp.view.terminal.get('terminalName'),
      "cashier_emp_id": OB.MobileApp.view.terminal.get('context').user.businessPartnerSearchKey,
      "cashier_name": OB.MobileApp.view.terminal.get('context').user.name
    };
    var ajaxRequest = new enyo.Ajax({
      url: OB.POS.hwserver.url.replace('/printer', '/process/MulticastService'),
      cacheBust: false,
      method: 'POST',
      handleAs: 'json',
      data: JSON.stringify(multicast_date),
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      success: function (inSender, inResponse) {
        console.log('Multicast service success')
        OB.UTIL.HookManager.callbackExecutor(args, c);
      },
      fail: function (inSender, inResponse) {
        console.log('Multicast service fail');
        OB.UTIL.HookManager.callbackExecutor(args, c);
      }
    });
    ajaxRequest.go(ajaxRequest.data).response('success').error('fail');
  };
});