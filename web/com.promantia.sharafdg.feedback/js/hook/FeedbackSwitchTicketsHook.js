OB.UTIL.HookManager.registerHook('OBPOS_RenderListReceiptLine', function (args, callbacks) {
  var order = args.listReceiptLine.model,
      count = 0;

  if (localStorage.getItem('receipt')) {
    localStorage.setItem('receipt_backup', localStorage.getItem('receipt'));
  }

  OB.CPSF = OB.CPSF || {};

  OB.CPSF.Utils = {

    getBrandAndDepartment: function (prod, callback) {
      var note = [];
      OB.Dal.find(OB.Model.Brand, {
        id: prod.get('product').get('brand')
      }, function (brand) {
        if (brand.length === 1) {
          note.brand = brand.at(0).get('name');
        } else {
          note.brand = '-';
        }
      });
      OB.CUSTSHA.findRootId({
        categoryId: prod.get('product').get('productCategory'),
        parentId: prod.get('product').get('productCategory')
      }).then(function (data) {
        note.department = data.category.get('searchKey');
        note.departmentName = data.category.get('name');
        callback(note);
      }, function (err) {
        note.department = '-';
        note.departmentName = '-';
        callback(note);
      });
    }
  };

  if ((localStorage.getItem('receipt') !== null) && (typeof localStorage.getItem('receipt') !== 'undefined')) {
    var productArr = [];
    var salesExecBPSK = OB.MobileApp.view.terminal.get('context').user.businessPartnerSearchKey;
    order.get('lines').models.forEach(function (prod) {
      OB.CPSF.Utils.getBrandAndDepartment(prod, function (note) {
        var prodObj = {
          "sku": prod.get('product').get('searchkey'),
          "productname": prod.get('product').get('_identifier'),
          "brand": note.brand,
          "dept_code": note.department,
          "dept_name": note.departmentName,
          "quantity": prod.get('qty'),
          "salesexecempid": prod.get('shasrSalesrepUserBPSk') ? prod.get('shasrSalesrepUserBPSk').split('-')[0] : salesExecBPSK
        };
        productArr.push(prodObj);
        count++;
        if (order.get('lines').models.length > 0 && order.get('lines').models.length === count) {
          if (!OB.UTIL.isNullOrUndefined(order) && !OB.UTIL.isNullOrUndefined(order.get('bp'))) {
            getSwitchOrderJSON(order, productArr, vat);
          }
        }
      });
    });

    var vat = 0;
    var taxes = order.get('taxes');
    for (var t in taxes) {
      vat += taxes[t].amount;
    }

    getSwitchOrderJSON = function (order, productArr, vat) {
      var finalObj = {
        "name": order.get('bp').get('name') ? order.get('bp').get('name') : order.get('bp').get('firstName'),
        "cashiername": OB.MobileApp.view.terminal.get('context').user.name,
        "mobile": order.get('bp').get('phone'),
        "category": order.get('bp').get('businessPartnerCategory_name'),
        "address": order.get('bp').get('locName'),
        "email": order.get('bp').get('email'),
        "invoicenumber": order.get('documentNo'),
        "invoicedate": moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD'),
        "products": productArr,
        "total": order.get('gross'),
        "vat": vat,
        "id_customer": order.get('bp').get('id'),
        "id_terminal": order.get('posTerminal'),
        "id_organization": order.get('bp').get('organization'),
        "organization": OB.MobileApp.view.terminal.get('context').organization.searchKey,
        "cashierempid": OB.MobileApp.view.terminal.get('context').user.businessPartnerSearchKey
      };

      localStorage.setItem('receipt', JSON.stringify(finalObj));
    };
  }
  localStorage.setItem('refresh', '0');
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});