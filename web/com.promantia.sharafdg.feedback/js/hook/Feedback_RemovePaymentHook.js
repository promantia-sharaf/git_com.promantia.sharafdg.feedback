OB.UTIL.HookManager.registerHook('OBPOS_preRemovePayment', function (args, callbacks) {

  if (localStorage.getItem('receipt')) {
    localStorage.setItem('receipt_backup', localStorage.getItem('receipt'));
  }

  var productArr = [],
      count = 0;
  args.receipt.get('lines').models.forEach(function (prod) {
    OB.CPSF.Utils.getBrandAndDepartment(prod, function (note) {
      var prodObj = {
        "sku": prod.get('product').get('searchkey'),
        "productname": prod.get('product').get('_identifier'),
        "brand": note.brand,
        "dept_code": note.department,
        "dept_name": note.departmentName,
        "quantity": prod.get('qty')
      };
      productArr.push(prodObj);
      count++;
      if (args.receipt.get('lines').models.length === count) {
        getOrderJSON(args, productArr, vat);
      }
    });
  });

  var vat = 0;
  var taxes = args.receipt.get('taxes');
  for (var t in taxes) {
    vat += taxes[t].amount;
  }

  getOrderJSON = function (args, productArr, vat) {
    var bpObj = {
      "name": args.receipt.get('bp').get('name'),
      "cashiername": OB.MobileApp.view.terminal.get('context').user.name,
      "mobile": args.receipt.get('bp').get('phone'),
      "category": args.receipt.get('bp').get('businessPartnerCategory_name'),
      "address": args.receipt.get('bp').get('locName'),
      "email": args.receipt.get('bp').get('email'),
      "products": productArr,
      "total": args.receipt.get('gross'),
      "vat": vat,
      "id_customer": args.receipt.get('bp').get('id'),
      "id_terminal": args.receipt.get('posTerminal'),
      "id_organization": args.receipt.get('bp').get('organization')
    };

    localStorage.setItem('refresh', '0');
    localStorage.setItem('receipt', JSON.stringify(bpObj));
  };
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);

});