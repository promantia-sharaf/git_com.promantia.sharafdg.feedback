/**
 * 
 */

OB.UTIL.HookManager.registerHook('OBPOS_LoadPOSWindow', function (args, callbacks) {
	
	if ( !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('context').user) && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('context').user.businessPartner) ){
	new OB.DS.Process('com.promantia.sharafdg.feedback.GetUserBPSearchKey').exec({
		bpId: OB.MobileApp.model.get('context').user.businessPartner
	}, function(data) {
		if (data && data.exception) {
				console.error(data.exception);
				console.info("Application Serer Not Able To Reach");
				OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		}
		else if (data){
			console.info('Users BP searchKey is :'+ data.bpsearchKey);
			if(!OB.UTIL.isNullOrUndefined(data.bpsearchKey) ){
			OB.MobileApp.model.get('context').user.businessPartnerSearchKey = data.bpsearchKey;
			}
			OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		}
	});
	}else {
		OB.UTIL.HookManager.callbackExecutor(args, callbacks);	
	}
});